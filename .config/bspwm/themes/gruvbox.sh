# Gruvbox theme
bspc config focused_border_color "#b8bb26"
bspc config active_border_color "#282828"
bspc config normal_border_color "#3c3836"

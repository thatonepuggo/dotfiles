"
"        _
" __   _(_)_ __ ___
" \ \ / / | '_ ` _ \
"  \   /| | | | | | | config
"   \_/ |_|_| |_| |_|
" by thatonepuggo
"

call plug#begin('~/.vim/plugged')
" syntax highlighting
Plug 'rust-lang/rust.vim'
Plug 'fladson/vim-kitty'
Plug 'kovetskiy/sxhkd-vim'
Plug 'waycrate/swhkd-vim'
Plug 'ziglang/zig.vim'
" lualine
Plug 'nvim-lualine/lualine.nvim'
Plug 'ryanoasis/vim-devicons'
" theme
Plug 'morhetz/gruvbox'
Plug 'luochen1990/rainbow'
" coc
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" fzf
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" goyo
Plug 'junegunn/goyo.vim'
call plug#end()

"
" syntax
"
syntax enable
filetype plugin indent on

"
" lua code
"
lua << END
-- lualine
require('lualine').setup({ options = {
  section_separators = { left = '', right = '' },
  component_separators = { left = '', right = '' }
} })
END

"
" colorscheme
"
colorscheme gruvbox

"
" lettings
"
" set the leader key
let mapleader=" "

"
" settings
"
" mouse
set mouse=a
" set both number and relativenumber
set number
set relativenumber
" set encoding to utf8
set encoding=UTF-8
" [TAB] -> 4 spaces (unless editing in makefile of course)
set tabstop=8
set softtabstop=0
set shiftwidth=4
set smarttab
set expandtab
" 80 column rule
set colorcolumn=80

" the cursor column
set cursorcolumn
" set the clipboard to use the system clipboard
set clipboard+=unnamedplus
" dont try to check for set statements
set nomodeline
" disable auto comments
set formatoptions-=cro

"
" gui lettings
"
let g:neovide_cursor_vfx_mode = "pixiedust"
let g:neovide_transparency = 0.975
"
" gui settings
"
set guifont=Iosevka:h15

"
" autocmds
"

" Center the screen entering insert mode
autocmd InsertEnter * norm zz

" Remove trailing whitespace before saving
autocmd BufWritePre * %s/\s\+$//e

"
" other stuff
"

" add :noh keybinding
nmap <leader>nh :noh<CR>

" goyo keybinding
nmap <leader><space> :Goyo<CR>

" remap control-r to U (redo)
nnoremap U <C-R>

"" netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_winsize = 20

" Split funcs
function! OpenToRight()
   :normal v
   let g:path = expand('%:p')
   :q!
   execute 'belowright vnew' g:path
   :normal <C-t>
endfunction

function! OpenBelow()
    :normal v
    let g:path = expand('%:p')
    :q!
    execute 'belowright new' g:path
    :normal <C-t>
endfunction

" Netrw mappings
function! NetrwMappings()
    noremap <silent> <C-t> :call ToggleNetrw()<CR>
    noremap <buffer> V :call OpenToRight()<CR>
    noremap <buffer> H :call OpenBelow()<CR>
endfunction

augroup netrw_mappings
    autocmd!
    autocmd filetype netrw call NetrwMappings()
augroup END

" Toggle Netrw
function! ToggleNetrw()
    if g:NetrwIsOpen
        let i = bufnr("$")
        while (i >= 1)
            if (getbufvar(i, "&filetype") == "netrw")
                silent exe "bwipeout " . i
            endif
            let i-=1
        endwhile
        let g:NetrwIsOpen=0
    else
        let g:NetrwIsOpen=1
        silent Lexplore
        exe 2 . "wincmd w"
    endif
endfunction

" Close netrw if it's the only buffer open
autocmd WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw" || &buftype == 'quickfix' |q|endif

" Project Draw
augroup ProjectDrawer
    autocmd!
    autocmd VimEnter * :call ToggleNetrw()
augroup END

" open netrw
let g:NetrwIsOpen=0


"" end of netrw config

" coc
nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gr <Plug>(coc-references)

" fzf
nnoremap <C-p> :GFiles<CR>

" Toggle auto comments
map <leader>c :setlocal formatoptions-=cro<CR>
map <leader>C :setlocal formatoptions=cro<CR>




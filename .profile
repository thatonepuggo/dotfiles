## FUNCTIONS ##
addPath() {
    export PATH="$1:$PATH"
}

## ENV VARS ##
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR='vim'
export TERM='xterm-256color'

## PATH ##
addPath "$HOME/.cabal/bin:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/.ghcup/bin"

#
#          _
#  _______| |__
# |_  / __| '_ \
#  / /\__ \ | | | config
# /___|___/_| |_|
# by thatonepuggo
#
antigen_dir=$HOME/.cache/antigen/

## VIM KEYS ##
bindkey -v
bindkey '^?' backward-delete-char

## PLUGINS ##
if [ ! -d "$antigen_dir" ]; then
    mkdir $antigen_dir
    curl -L git.io/antigen > $antigen_dir/antigen.zsh
fi

source $antigen_dir/antigen.zsh

antigen bundle 'zsh-users/zsh-syntax-highlighting'
antigen bundle 'zsh-users/zsh-autosuggestions'
antigen apply

## ALIASES ##
exa_args="exa --icons --group-directories-first"
alias ls="$exa_args -lh"
alias ll="$exa_args -lah"
alias lt="$exa_args -lah --tree"
# vim -> nvim && extra options for neovide
alias vim='nvim'
alias vide='neovide --multigrid'
# doom emacs
[ -f "$HOME/.emacs.d/bin/doom" ] && alias doom="$HOME/.emacs.d/bin/doom"
# cat -> bat
alias cat='bat'
# git bare repository
alias config="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"
# grep
alias grep='rg --color auto'
# clear
alias clear='echo -en "\x1b[2J\x1b[1;1H"'

## SOURCING ##
source $HOME/.profile

## PROMPT ##
eval `starship init zsh`

## PFETCH ##
pfetch
